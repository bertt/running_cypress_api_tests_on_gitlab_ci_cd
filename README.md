# Running Cypress Api Tests On Gitlab Ci Cd
# Running Cypress API Tests on GitLab CI/CD

## Create webservice

Create a service that returns "Hello World!"

```
$ cd server
$ dotnet new web
$ dotnet run
Building...
info: Microsoft.Hosting.Lifetime[14]
      Now listening on: https://localhost:7287
info: Microsoft.Hosting.Lifetime[14]
      Now listening on: http://localhost:5269
info: Microsoft.Hosting.Lifetime[0]
      Application started. Press Ctrl+C to shut down.
info: Microsoft.Hosting.Lifetime[0]
      Hosting environment: Development
info: Microsoft.Hosting.Lifetime[0]
      Content root path: D:\dev\gitlab.com\bertt\running_cypress_api_tests_on_gitlab_ci_cd\server\
```

Inspect service with cURL:

```
$ curl https://localhost:7287/
Hello World!
```
## Create Cypress

```
$ npm i cypress --save-dev
```

Start Cypress

```
$ npx cypress open
```

A Cypress folder will be created

Open package.json and add a script:

```
  "scripts": {
    "test:prod": "cypress run --config baseUrl=http://localhost:5269"
  },
```

Add a test.js file to integration folder containing:

```
describe('root', () => {
    it('Get root', () => {
       cy.request('/')
       .then((response) => {
           expect(response.body).to.equal('Hello World!')
        })
     })
 })
 ```

## Create GitLab CI/CD file

```
stages:
    - building
    - cypress-tests

building:
    image: mcr.microsoft.com/dotnet/sdk:6.0
    stage: building
    script:
        - dotnet publish -c Release -r linux-x64 -o publish-server server/server.csproj
    artifacts:
        paths:
            - publish-server/    

cypress-tests:  
    stage: cypress-tests
    image: cypress/base
    allow_failure: true
    script:
            # start server
        - cd publish-server
        - ./server &
        # start api
        - cd ../api-testing
        - npm i
        - ./node_modules/.bin/cypress install

        # run cypress tests
        - npm run runtests
```




