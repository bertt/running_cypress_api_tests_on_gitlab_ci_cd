describe('root', () => {
    it('Get root', () => {
       cy.request('/')
       .then((response) => {
           expect(response.body).to.equal('Hello World!')
        })
     })
 })